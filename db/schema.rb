# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_06_195548) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "employees", force: :cascade do |t|
    t.string "matricula"
    t.integer "numfunc"
    t.integer "numvinc"
    t.string "nome"
    t.string "sexo"
    t.string "numrg"
    t.string "orgaorg"
    t.string "ufrg"
    t.string "cpf"
    t.date "data_admissao"
    t.string "telefone"
    t.string "endereco"
    t.string "bairro"
    t.string "cidade"
    t.string "cep"
    t.string "uf"
    t.date "data_nascimento"
    t.date "data_base_ferias"
    t.string "email"
    t.string "cargo"
    t.string "funcao"
    t.string "orgao"
    t.integer "cod_orgao"
    t.decimal "salario"
    t.decimal "gratificacao"
    t.string "tipo_vinculo"
    t.date "data_demissao"
    t.date "data_fim_contrato"
    t.string "escolaridade"
    t.string "situacao"
    t.string "referencia_cargo"
    t.string "pis"
    t.string "referencia_funcao"
    t.date "dtini_ferias"
    t.date "dtfim_ferias"
    t.string "sigla_orgao"
    t.string "agencia"
    t.string "conta"
    t.string "banco"
    t.string "situacao_ferias"
    t.string "cnpj_orgao"
    t.string "poder"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fake_secads", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "entity_id"
    t.string "matricula"
    t.string "numfunc"
    t.string "numvinc"
    t.string "sexo"
    t.string "numrg"
    t.string "orgaorg"
    t.string "ufrg"
    t.date "data_admissao"
    t.string "telefone"
    t.string "endereco"
    t.string "bairro"
    t.string "cidade"
    t.string "cep"
    t.string "uf"
    t.date "data_nascimento"
    t.date "data_base_ferias"
    t.string "cargo"
    t.string "funcao"
    t.string "salario"
    t.string "gratificacao"
    t.string "tipo_vinculo"
    t.date "data_demissao"
    t.date "data_fim_contrato"
    t.string "escolaridade"
    t.string "situacao"
    t.string "referencia_cargo_"
    t.string "pis"
    t.string "referencia_funcao_"
    t.date "dtini_ferias"
    t.date "dtfim_ferias"
    t.string "agencia"
    t.string "conta"
    t.string "banco"
    t.string "situacao_ferias"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "orgao"
    t.index ["entity_id"], name: "index_fake_secads_on_entity_id"
    t.index ["user_id"], name: "index_fake_secads_on_user_id"
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer "resource_owner_id", null: false
    t.integer "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer "resource_owner_id"
    t.integer "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "confidential", default: true, null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "cpf"
    t.string "matricula"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.text "photo"
    t.string "cloudinary_code"
    t.boolean "bypass_validation"
    t.string "perfil"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
