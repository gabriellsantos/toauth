class AddCloudinaryCodeToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :cloudinary_code, :string
  end
end
