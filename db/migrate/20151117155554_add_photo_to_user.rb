class AddPhotoToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :photo, :text
  end
end
