class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :matricula
      t.integer :numfunc
      t.integer :numvinc
      t.string :nome
      t.string :sexo
      t.string :numrg
      t.string :orgaorg
      t.string :ufrg
      t.string :cpf
      t.date :data_admissao
      t.string :telefone
      t.string :endereco
      t.string :bairro
      t.string :cidade
      t.string :cep
      t.string :uf
      t.date :data_nascimento
      t.date :data_base_ferias
      t.string :email
      t.string :cargo
      t.string :funcao
      t.string :orgao
      t.integer :cod_orgao
      t.decimal :salario
      t.decimal :gratificacao
      t.string :tipo_vinculo
      t.date :data_demissao
      t.date :data_fim_contrato
      t.string :escolaridade
      t.string :situacao
      t.string :referencia_cargo
      t.string :pis
      t.string :referencia_funcao
      t.date :dtini_ferias
      t.date :dtfim_ferias
      t.string :sigla_orgao
      t.string :agencia
      t.string :conta
      t.string :banco
      t.string :situacao_ferias
      t.string :cnpj_orgao
      t.string :poder

      t.timestamps
    end
  end
end
