class AddBypassValidationToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :bypass_validation, :boolean
  end
end
