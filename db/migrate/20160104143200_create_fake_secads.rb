class CreateFakeSecads < ActiveRecord::Migration[5.1]
  def change
    create_table :fake_secads do |t|
      t.references :user, index: true
      t.references :entity, index: true
      t.string :matricula
      t.string :numfunc
      t.string :numvinc
      t.string :sexo
      t.string :numrg
      t.string :orgaorg
      t.string :ufrg
      t.date :data_admissao
      t.string :telefone
      t.string :endereco
      t.string :bairro
      t.string :cidade
      t.string :cep
      t.string :uf
      t.date :data_nascimento
      t.date :data_base_ferias
      t.string :cargo
      t.string :funcao
      t.string :salario
      t.string :gratificacao
      t.string :tipo_vinculo
      t.date :data_demissao
      t.date :data_fim_contrato
      t.string :escolaridade
      t.string :situacao
      t.string :referencia_cargo_
      t.string :pis
      t.string :referencia_funcao_
      t.date :dtini_ferias
      t.date :dtfim_ferias
      t.string :agencia
      t.string :conta
      t.string :banco
      t.string :situacao_ferias

      t.timestamps
    end
  end
end
