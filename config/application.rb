require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Toauth
    class Application < Rails::Application
        # Settings in config/environments/* take precedence over those specified here.
        # Application configuration should go into files in config/initializers
        # -- all .rb files in that directory are automatically loaded.

        # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
        # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
        # config.time_zone = 'Central Time (US & Canada)'

        # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
        # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
        # config.i18n.default_locale = :de
        config.time_zone = 'Buenos Aires'
        config.i18n.available_locales = [:en, :"pt-BR"]
        config.i18n.default_locale = :"pt-BR"
        I18n.config.enforce_available_locales = false
        config.encoding = "utf-8"
        #config.filter_parameters << :password

        Time::DATE_FORMATS[:default] = "%d/%m/%Y %H:%M"
        Date::DATE_FORMATS[:default] = "%d/%m/%Y"

        config.to_prepare do
          Devise::Mailer.layout "email" # email.haml or email.erb
        end

        config.middleware.insert_before 0, Rack::Cors do
            allow do
                origins '*'
                resource '*', headers: :any, methods: [:get, :post, :options]
            end
        end
end
end
