Rails.application.routes.draw do
  resources :employees
  get 'alerts/index', as: :alert

  resources :fake_secads

  get 'api/me'
  get 'api/me/profile' => "api#profile"
  get 'api/departments_by_ug'
  get 'api/servers_by_ug'
  post 'api/sign_by_password'
  post 'api/get_token'

  use_doorkeeper

  devise_for :users
  resources :users do
    member do
      patch "change_avatar"
    end
    collection do
      get "clear_coockie"
      post "autenticar_service"
      get "login_secad"
    end
  end

  get "minha_pagina" => "users#minha_pagina", as: :minha_pagina
  get "meus_dados" => "users#dados_api", as: :meus_dados
  get "password" => "users#password", as: :minha_senha
  get "senha_sucesso" => "users#senha_sucesso", as: :senha_sucesso
  post "password_change" => "users#password_change"

  authenticated :user do
    root "users#minha_pagina"
  end
  unauthenticated :user do
    devise_scope :user do
      get "/" => "devise/sessions#new"
    end
  end

  scope "/admin" do
    resources :users
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
