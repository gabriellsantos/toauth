$(function()
{
  /*
     * Multiselect
     */
    $('#multiselect-optgroup').multiSelect({ selectableOptgroup: true });
    $('#pre-selected-options').multiSelect();
    $('.multiselect-custom').multiSelect({
    	selectableHeader: "<div class='custom-header'>Selecionáveis</div>",
    	selectionHeader: "<div class='custom-header'>Selecionados</div>",
    	selectableFooter: "<div class='custom-header custom-footer'>Selecionáveis</div>",
    	selectionFooter: "<div class='custom-header custom-footer'>Selecionados</div>"
    });
    
});