FROM alutsu/ruby-node-yarn

# Install Ruby Gems and node modules
COPY Gemfile* /tmp/
# COPY package.json /tmp/
# COPY yarn.lock /tmp/
WORKDIR /tmp
RUN gem install bundler
RUN bundle install --jobs 5 --retry 5 --without development test
RUN yarn install
RUN mkdir /app
WORKDIR /app
COPY . /app
ENV RAILS_ENV production

# Execute the Procfile
CMD ["bin/run-dev.sh"]