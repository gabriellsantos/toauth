require "rails_helper"

RSpec.describe FakeSecadsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/fake_secads").to route_to("fake_secads#index")
    end

    it "routes to #new" do
      expect(:get => "/fake_secads/new").to route_to("fake_secads#new")
    end

    it "routes to #show" do
      expect(:get => "/fake_secads/1").to route_to("fake_secads#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/fake_secads/1/edit").to route_to("fake_secads#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/fake_secads").to route_to("fake_secads#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/fake_secads/1").to route_to("fake_secads#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/fake_secads/1").to route_to("fake_secads#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/fake_secads/1").to route_to("fake_secads#destroy", :id => "1")
    end

  end
end
