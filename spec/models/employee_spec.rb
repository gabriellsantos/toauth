# == Schema Information
#
# Table name: employees
#
#  id                :integer          not null, primary key
#  agencia           :string
#  bairro            :string
#  banco             :string
#  cargo             :string
#  cep               :string
#  cidade            :string
#  cnpj_orgao        :string
#  cod_orgao         :integer
#  conta             :string
#  cpf               :string
#  data_admissao     :date
#  data_base_ferias  :date
#  data_demissao     :date
#  data_fim_contrato :date
#  data_nascimento   :date
#  dtfim_ferias      :date
#  dtini_ferias      :date
#  email             :string
#  endereco          :string
#  escolaridade      :string
#  funcao            :string
#  gratificacao      :decimal(, )
#  matricula         :string
#  nome              :string
#  numfunc           :integer
#  numrg             :string
#  numvinc           :integer
#  orgao             :string
#  orgaorg           :string
#  pis               :string
#  poder             :string
#  referencia_cargo  :string
#  referencia_funcao :string
#  salario           :decimal(, )
#  sexo              :string
#  sigla_orgao       :string
#  situacao          :string
#  situacao_ferias   :string
#  telefone          :string
#  tipo_vinculo      :string
#  uf                :string
#  ufrg              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'rails_helper'

RSpec.describe Employee, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
