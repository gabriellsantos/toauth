require 'rails_helper'

RSpec.describe "fake_secads/index", type: :view do
  before(:each) do
    assign(:fake_secads, [
      FakeSecad.create!(
        :user => nil,
        :entity => nil,
        :matricula => "Matricula",
        :numfunc => "Numfunc",
        :numvinc => "Numvinc",
        :sexo => "Sexo",
        :numrg => "Numrg",
        :orgaorg => "Orgaorg",
        :ufrg => "Ufrg",
        :telefone => "Telefone",
        :endereco => "Endereco",
        :bairro => "Bairro",
        :cidade => "Cidade",
        :cep => "Cep",
        :uf => "Uf",
        :cargo => "Cargo",
        :funcao => "Funcao",
        :salario => "Salario",
        :gratificacao => "Gratificacao",
        :tipo_vinculo => "Tipo Vinculo",
        :escolaridade => "Escolaridade",
        :situacao => "Situacao",
        :referencia_cargo_ => "Referencia Cargo ",
        :pis => "Pis",
        :referencia_funcao_ => "Referencia Funcao ",
        :agencia => "Agencia",
        :conta => "Conta",
        :banco => "Banco",
        :situacao_ferias => "Situacao Ferias"
      ),
      FakeSecad.create!(
        :user => nil,
        :entity => nil,
        :matricula => "Matricula",
        :numfunc => "Numfunc",
        :numvinc => "Numvinc",
        :sexo => "Sexo",
        :numrg => "Numrg",
        :orgaorg => "Orgaorg",
        :ufrg => "Ufrg",
        :telefone => "Telefone",
        :endereco => "Endereco",
        :bairro => "Bairro",
        :cidade => "Cidade",
        :cep => "Cep",
        :uf => "Uf",
        :cargo => "Cargo",
        :funcao => "Funcao",
        :salario => "Salario",
        :gratificacao => "Gratificacao",
        :tipo_vinculo => "Tipo Vinculo",
        :escolaridade => "Escolaridade",
        :situacao => "Situacao",
        :referencia_cargo_ => "Referencia Cargo ",
        :pis => "Pis",
        :referencia_funcao_ => "Referencia Funcao ",
        :agencia => "Agencia",
        :conta => "Conta",
        :banco => "Banco",
        :situacao_ferias => "Situacao Ferias"
      )
    ])
  end

  it "renders a list of fake_secads" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Matricula".to_s, :count => 2
    assert_select "tr>td", :text => "Numfunc".to_s, :count => 2
    assert_select "tr>td", :text => "Numvinc".to_s, :count => 2
    assert_select "tr>td", :text => "Sexo".to_s, :count => 2
    assert_select "tr>td", :text => "Numrg".to_s, :count => 2
    assert_select "tr>td", :text => "Orgaorg".to_s, :count => 2
    assert_select "tr>td", :text => "Ufrg".to_s, :count => 2
    assert_select "tr>td", :text => "Telefone".to_s, :count => 2
    assert_select "tr>td", :text => "Endereco".to_s, :count => 2
    assert_select "tr>td", :text => "Bairro".to_s, :count => 2
    assert_select "tr>td", :text => "Cidade".to_s, :count => 2
    assert_select "tr>td", :text => "Cep".to_s, :count => 2
    assert_select "tr>td", :text => "Uf".to_s, :count => 2
    assert_select "tr>td", :text => "Cargo".to_s, :count => 2
    assert_select "tr>td", :text => "Funcao".to_s, :count => 2
    assert_select "tr>td", :text => "Salario".to_s, :count => 2
    assert_select "tr>td", :text => "Gratificacao".to_s, :count => 2
    assert_select "tr>td", :text => "Tipo Vinculo".to_s, :count => 2
    assert_select "tr>td", :text => "Escolaridade".to_s, :count => 2
    assert_select "tr>td", :text => "Situacao".to_s, :count => 2
    assert_select "tr>td", :text => "Referencia Cargo ".to_s, :count => 2
    assert_select "tr>td", :text => "Pis".to_s, :count => 2
    assert_select "tr>td", :text => "Referencia Funcao ".to_s, :count => 2
    assert_select "tr>td", :text => "Agencia".to_s, :count => 2
    assert_select "tr>td", :text => "Conta".to_s, :count => 2
    assert_select "tr>td", :text => "Banco".to_s, :count => 2
    assert_select "tr>td", :text => "Situacao Ferias".to_s, :count => 2
  end
end
