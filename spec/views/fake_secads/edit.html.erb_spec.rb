require 'rails_helper'

RSpec.describe "fake_secads/edit", type: :view do
  before(:each) do
    @fake_secad = assign(:fake_secad, FakeSecad.create!(
      :user => nil,
      :entity => nil,
      :matricula => "MyString",
      :numfunc => "MyString",
      :numvinc => "MyString",
      :sexo => "MyString",
      :numrg => "MyString",
      :orgaorg => "MyString",
      :ufrg => "MyString",
      :telefone => "MyString",
      :endereco => "MyString",
      :bairro => "MyString",
      :cidade => "MyString",
      :cep => "MyString",
      :uf => "MyString",
      :cargo => "MyString",
      :funcao => "MyString",
      :salario => "MyString",
      :gratificacao => "MyString",
      :tipo_vinculo => "MyString",
      :escolaridade => "MyString",
      :situacao => "MyString",
      :referencia_cargo_ => "MyString",
      :pis => "MyString",
      :referencia_funcao_ => "MyString",
      :agencia => "MyString",
      :conta => "MyString",
      :banco => "MyString",
      :situacao_ferias => "MyString"
    ))
  end

  it "renders the edit fake_secad form" do
    render

    assert_select "form[action=?][method=?]", fake_secad_path(@fake_secad), "post" do

      assert_select "input#fake_secad_user_id[name=?]", "fake_secad[user_id]"

      assert_select "input#fake_secad_entity_id[name=?]", "fake_secad[entity_id]"

      assert_select "input#fake_secad_matricula[name=?]", "fake_secad[matricula]"

      assert_select "input#fake_secad_numfunc[name=?]", "fake_secad[numfunc]"

      assert_select "input#fake_secad_numvinc[name=?]", "fake_secad[numvinc]"

      assert_select "input#fake_secad_sexo[name=?]", "fake_secad[sexo]"

      assert_select "input#fake_secad_numrg[name=?]", "fake_secad[numrg]"

      assert_select "input#fake_secad_orgaorg[name=?]", "fake_secad[orgaorg]"

      assert_select "input#fake_secad_ufrg[name=?]", "fake_secad[ufrg]"

      assert_select "input#fake_secad_telefone[name=?]", "fake_secad[telefone]"

      assert_select "input#fake_secad_endereco[name=?]", "fake_secad[endereco]"

      assert_select "input#fake_secad_bairro[name=?]", "fake_secad[bairro]"

      assert_select "input#fake_secad_cidade[name=?]", "fake_secad[cidade]"

      assert_select "input#fake_secad_cep[name=?]", "fake_secad[cep]"

      assert_select "input#fake_secad_uf[name=?]", "fake_secad[uf]"

      assert_select "input#fake_secad_cargo[name=?]", "fake_secad[cargo]"

      assert_select "input#fake_secad_funcao[name=?]", "fake_secad[funcao]"

      assert_select "input#fake_secad_salario[name=?]", "fake_secad[salario]"

      assert_select "input#fake_secad_gratificacao[name=?]", "fake_secad[gratificacao]"

      assert_select "input#fake_secad_tipo_vinculo[name=?]", "fake_secad[tipo_vinculo]"

      assert_select "input#fake_secad_escolaridade[name=?]", "fake_secad[escolaridade]"

      assert_select "input#fake_secad_situacao[name=?]", "fake_secad[situacao]"

      assert_select "input#fake_secad_referencia_cargo_[name=?]", "fake_secad[referencia_cargo_]"

      assert_select "input#fake_secad_pis[name=?]", "fake_secad[pis]"

      assert_select "input#fake_secad_referencia_funcao_[name=?]", "fake_secad[referencia_funcao_]"

      assert_select "input#fake_secad_agencia[name=?]", "fake_secad[agencia]"

      assert_select "input#fake_secad_conta[name=?]", "fake_secad[conta]"

      assert_select "input#fake_secad_banco[name=?]", "fake_secad[banco]"

      assert_select "input#fake_secad_situacao_ferias[name=?]", "fake_secad[situacao_ferias]"
    end
  end
end
