require 'rails_helper'

RSpec.describe "fake_secads/show", type: :view do
  before(:each) do
    @fake_secad = assign(:fake_secad, FakeSecad.create!(
      :user => nil,
      :entity => nil,
      :matricula => "Matricula",
      :numfunc => "Numfunc",
      :numvinc => "Numvinc",
      :sexo => "Sexo",
      :numrg => "Numrg",
      :orgaorg => "Orgaorg",
      :ufrg => "Ufrg",
      :telefone => "Telefone",
      :endereco => "Endereco",
      :bairro => "Bairro",
      :cidade => "Cidade",
      :cep => "Cep",
      :uf => "Uf",
      :cargo => "Cargo",
      :funcao => "Funcao",
      :salario => "Salario",
      :gratificacao => "Gratificacao",
      :tipo_vinculo => "Tipo Vinculo",
      :escolaridade => "Escolaridade",
      :situacao => "Situacao",
      :referencia_cargo_ => "Referencia Cargo ",
      :pis => "Pis",
      :referencia_funcao_ => "Referencia Funcao ",
      :agencia => "Agencia",
      :conta => "Conta",
      :banco => "Banco",
      :situacao_ferias => "Situacao Ferias"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Matricula/)
    expect(rendered).to match(/Numfunc/)
    expect(rendered).to match(/Numvinc/)
    expect(rendered).to match(/Sexo/)
    expect(rendered).to match(/Numrg/)
    expect(rendered).to match(/Orgaorg/)
    expect(rendered).to match(/Ufrg/)
    expect(rendered).to match(/Telefone/)
    expect(rendered).to match(/Endereco/)
    expect(rendered).to match(/Bairro/)
    expect(rendered).to match(/Cidade/)
    expect(rendered).to match(/Cep/)
    expect(rendered).to match(/Uf/)
    expect(rendered).to match(/Cargo/)
    expect(rendered).to match(/Funcao/)
    expect(rendered).to match(/Salario/)
    expect(rendered).to match(/Gratificacao/)
    expect(rendered).to match(/Tipo Vinculo/)
    expect(rendered).to match(/Escolaridade/)
    expect(rendered).to match(/Situacao/)
    expect(rendered).to match(/Referencia Cargo /)
    expect(rendered).to match(/Pis/)
    expect(rendered).to match(/Referencia Funcao /)
    expect(rendered).to match(/Agencia/)
    expect(rendered).to match(/Conta/)
    expect(rendered).to match(/Banco/)
    expect(rendered).to match(/Situacao Ferias/)
  end
end
