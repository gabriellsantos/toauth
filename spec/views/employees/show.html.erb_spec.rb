require 'rails_helper'

RSpec.describe "employees/show", type: :view do
  before(:each) do
    @employee = assign(:employee, Employee.create!(
      :matricula => "Matricula",
      :numfunc => 2,
      :numvinc => 3,
      :nome => "Nome",
      :sexo => "Sexo",
      :numrg => "Numrg",
      :orgaorg => "Orgaorg",
      :ufrg => "Ufrg",
      :cpf => "Cpf",
      :telefone => "Telefone",
      :endereco => "Endereco",
      :bairro => "Bairro",
      :cidade => "Cidade",
      :cep => "Cep",
      :uf => "Uf",
      :email => "Email",
      :cargo => "Cargo",
      :funcao => "Funcao",
      :orgao => "Orgao",
      :cod_orgao => 4,
      :salario => "9.99",
      :gratificacao => "9.99",
      :tipo_vinculo => "Tipo Vinculo",
      :escolaridade => "Escolaridade",
      :situacao => "Situacao",
      :referencia_cargo => "Referencia Cargo",
      :pis => "Pis",
      :referencia_funcao => "Referencia Funcao",
      :sigla_orgao => "Sigla Orgao",
      :agencia => "Agencia",
      :conta => "Conta",
      :banco => "Banco",
      :situacao_ferias => "Situacao Ferias",
      :cnpj_orgao => "Cnpj Orgao",
      :poder => "Poder"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Matricula/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Nome/)
    expect(rendered).to match(/Sexo/)
    expect(rendered).to match(/Numrg/)
    expect(rendered).to match(/Orgaorg/)
    expect(rendered).to match(/Ufrg/)
    expect(rendered).to match(/Cpf/)
    expect(rendered).to match(/Telefone/)
    expect(rendered).to match(/Endereco/)
    expect(rendered).to match(/Bairro/)
    expect(rendered).to match(/Cidade/)
    expect(rendered).to match(/Cep/)
    expect(rendered).to match(/Uf/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Cargo/)
    expect(rendered).to match(/Funcao/)
    expect(rendered).to match(/Orgao/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/Tipo Vinculo/)
    expect(rendered).to match(/Escolaridade/)
    expect(rendered).to match(/Situacao/)
    expect(rendered).to match(/Referencia Cargo/)
    expect(rendered).to match(/Pis/)
    expect(rendered).to match(/Referencia Funcao/)
    expect(rendered).to match(/Sigla Orgao/)
    expect(rendered).to match(/Agencia/)
    expect(rendered).to match(/Conta/)
    expect(rendered).to match(/Banco/)
    expect(rendered).to match(/Situacao Ferias/)
    expect(rendered).to match(/Cnpj Orgao/)
    expect(rendered).to match(/Poder/)
  end
end
