require 'rails_helper'

RSpec.describe "employees/index", type: :view do
  before(:each) do
    assign(:employees, [
      Employee.create!(
        :matricula => "Matricula",
        :numfunc => 2,
        :numvinc => 3,
        :nome => "Nome",
        :sexo => "Sexo",
        :numrg => "Numrg",
        :orgaorg => "Orgaorg",
        :ufrg => "Ufrg",
        :cpf => "Cpf",
        :telefone => "Telefone",
        :endereco => "Endereco",
        :bairro => "Bairro",
        :cidade => "Cidade",
        :cep => "Cep",
        :uf => "Uf",
        :email => "Email",
        :cargo => "Cargo",
        :funcao => "Funcao",
        :orgao => "Orgao",
        :cod_orgao => 4,
        :salario => "9.99",
        :gratificacao => "9.99",
        :tipo_vinculo => "Tipo Vinculo",
        :escolaridade => "Escolaridade",
        :situacao => "Situacao",
        :referencia_cargo => "Referencia Cargo",
        :pis => "Pis",
        :referencia_funcao => "Referencia Funcao",
        :sigla_orgao => "Sigla Orgao",
        :agencia => "Agencia",
        :conta => "Conta",
        :banco => "Banco",
        :situacao_ferias => "Situacao Ferias",
        :cnpj_orgao => "Cnpj Orgao",
        :poder => "Poder"
      ),
      Employee.create!(
        :matricula => "Matricula",
        :numfunc => 2,
        :numvinc => 3,
        :nome => "Nome",
        :sexo => "Sexo",
        :numrg => "Numrg",
        :orgaorg => "Orgaorg",
        :ufrg => "Ufrg",
        :cpf => "Cpf",
        :telefone => "Telefone",
        :endereco => "Endereco",
        :bairro => "Bairro",
        :cidade => "Cidade",
        :cep => "Cep",
        :uf => "Uf",
        :email => "Email",
        :cargo => "Cargo",
        :funcao => "Funcao",
        :orgao => "Orgao",
        :cod_orgao => 4,
        :salario => "9.99",
        :gratificacao => "9.99",
        :tipo_vinculo => "Tipo Vinculo",
        :escolaridade => "Escolaridade",
        :situacao => "Situacao",
        :referencia_cargo => "Referencia Cargo",
        :pis => "Pis",
        :referencia_funcao => "Referencia Funcao",
        :sigla_orgao => "Sigla Orgao",
        :agencia => "Agencia",
        :conta => "Conta",
        :banco => "Banco",
        :situacao_ferias => "Situacao Ferias",
        :cnpj_orgao => "Cnpj Orgao",
        :poder => "Poder"
      )
    ])
  end

  it "renders a list of employees" do
    render
    assert_select "tr>td", :text => "Matricula".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Nome".to_s, :count => 2
    assert_select "tr>td", :text => "Sexo".to_s, :count => 2
    assert_select "tr>td", :text => "Numrg".to_s, :count => 2
    assert_select "tr>td", :text => "Orgaorg".to_s, :count => 2
    assert_select "tr>td", :text => "Ufrg".to_s, :count => 2
    assert_select "tr>td", :text => "Cpf".to_s, :count => 2
    assert_select "tr>td", :text => "Telefone".to_s, :count => 2
    assert_select "tr>td", :text => "Endereco".to_s, :count => 2
    assert_select "tr>td", :text => "Bairro".to_s, :count => 2
    assert_select "tr>td", :text => "Cidade".to_s, :count => 2
    assert_select "tr>td", :text => "Cep".to_s, :count => 2
    assert_select "tr>td", :text => "Uf".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Cargo".to_s, :count => 2
    assert_select "tr>td", :text => "Funcao".to_s, :count => 2
    assert_select "tr>td", :text => "Orgao".to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Tipo Vinculo".to_s, :count => 2
    assert_select "tr>td", :text => "Escolaridade".to_s, :count => 2
    assert_select "tr>td", :text => "Situacao".to_s, :count => 2
    assert_select "tr>td", :text => "Referencia Cargo".to_s, :count => 2
    assert_select "tr>td", :text => "Pis".to_s, :count => 2
    assert_select "tr>td", :text => "Referencia Funcao".to_s, :count => 2
    assert_select "tr>td", :text => "Sigla Orgao".to_s, :count => 2
    assert_select "tr>td", :text => "Agencia".to_s, :count => 2
    assert_select "tr>td", :text => "Conta".to_s, :count => 2
    assert_select "tr>td", :text => "Banco".to_s, :count => 2
    assert_select "tr>td", :text => "Situacao Ferias".to_s, :count => 2
    assert_select "tr>td", :text => "Cnpj Orgao".to_s, :count => 2
    assert_select "tr>td", :text => "Poder".to_s, :count => 2
  end
end
