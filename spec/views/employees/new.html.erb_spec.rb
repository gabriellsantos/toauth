require 'rails_helper'

RSpec.describe "employees/new", type: :view do
  before(:each) do
    assign(:employee, Employee.new(
      :matricula => "MyString",
      :numfunc => 1,
      :numvinc => 1,
      :nome => "MyString",
      :sexo => "MyString",
      :numrg => "MyString",
      :orgaorg => "MyString",
      :ufrg => "MyString",
      :cpf => "MyString",
      :telefone => "MyString",
      :endereco => "MyString",
      :bairro => "MyString",
      :cidade => "MyString",
      :cep => "MyString",
      :uf => "MyString",
      :email => "MyString",
      :cargo => "MyString",
      :funcao => "MyString",
      :orgao => "MyString",
      :cod_orgao => 1,
      :salario => "9.99",
      :gratificacao => "9.99",
      :tipo_vinculo => "MyString",
      :escolaridade => "MyString",
      :situacao => "MyString",
      :referencia_cargo => "MyString",
      :pis => "MyString",
      :referencia_funcao => "MyString",
      :sigla_orgao => "MyString",
      :agencia => "MyString",
      :conta => "MyString",
      :banco => "MyString",
      :situacao_ferias => "MyString",
      :cnpj_orgao => "MyString",
      :poder => "MyString"
    ))
  end

  it "renders new employee form" do
    render

    assert_select "form[action=?][method=?]", employees_path, "post" do

      assert_select "input[name=?]", "employee[matricula]"

      assert_select "input[name=?]", "employee[numfunc]"

      assert_select "input[name=?]", "employee[numvinc]"

      assert_select "input[name=?]", "employee[nome]"

      assert_select "input[name=?]", "employee[sexo]"

      assert_select "input[name=?]", "employee[numrg]"

      assert_select "input[name=?]", "employee[orgaorg]"

      assert_select "input[name=?]", "employee[ufrg]"

      assert_select "input[name=?]", "employee[cpf]"

      assert_select "input[name=?]", "employee[telefone]"

      assert_select "input[name=?]", "employee[endereco]"

      assert_select "input[name=?]", "employee[bairro]"

      assert_select "input[name=?]", "employee[cidade]"

      assert_select "input[name=?]", "employee[cep]"

      assert_select "input[name=?]", "employee[uf]"

      assert_select "input[name=?]", "employee[email]"

      assert_select "input[name=?]", "employee[cargo]"

      assert_select "input[name=?]", "employee[funcao]"

      assert_select "input[name=?]", "employee[orgao]"

      assert_select "input[name=?]", "employee[cod_orgao]"

      assert_select "input[name=?]", "employee[salario]"

      assert_select "input[name=?]", "employee[gratificacao]"

      assert_select "input[name=?]", "employee[tipo_vinculo]"

      assert_select "input[name=?]", "employee[escolaridade]"

      assert_select "input[name=?]", "employee[situacao]"

      assert_select "input[name=?]", "employee[referencia_cargo]"

      assert_select "input[name=?]", "employee[pis]"

      assert_select "input[name=?]", "employee[referencia_funcao]"

      assert_select "input[name=?]", "employee[sigla_orgao]"

      assert_select "input[name=?]", "employee[agencia]"

      assert_select "input[name=?]", "employee[conta]"

      assert_select "input[name=?]", "employee[banco]"

      assert_select "input[name=?]", "employee[situacao_ferias]"

      assert_select "input[name=?]", "employee[cnpj_orgao]"

      assert_select "input[name=?]", "employee[poder]"
    end
  end
end
