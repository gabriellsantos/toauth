require 'rails_helper'

RSpec.describe "users/new", type: :view do
  before(:each) do
    assign(:user, User.new(
      :name => "MyString",
      :cpf => "MyString",
      :matricula => "MyString"
    ))
  end

  it "renders new user form" do
    render

    assert_select "form[action=?][method=?]", users_path, "post" do

      assert_select "input#user_name[name=?]", "user[name]"

      assert_select "input#user_cpf[name=?]", "user[cpf]"

      assert_select "input#user_matricula[name=?]", "user[matricula]"
    end
  end
end
