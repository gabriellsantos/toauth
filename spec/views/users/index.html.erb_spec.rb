require 'rails_helper'

RSpec.describe "users/index", type: :view do
  before(:each) do
    assign(:users, [
      User.create!(
        :name => "Name",
        :cpf => "Cpf",
        :matricula => "Matricula"
      ),
      User.create!(
        :name => "Name",
        :cpf => "Cpf",
        :matricula => "Matricula"
      )
    ])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Cpf".to_s, :count => 2
    assert_select "tr>td", :text => "Matricula".to_s, :count => 2
  end
end
