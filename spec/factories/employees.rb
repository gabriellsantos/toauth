# == Schema Information
#
# Table name: employees
#
#  id                :integer          not null, primary key
#  agencia           :string
#  bairro            :string
#  banco             :string
#  cargo             :string
#  cep               :string
#  cidade            :string
#  cnpj_orgao        :string
#  cod_orgao         :integer
#  conta             :string
#  cpf               :string
#  data_admissao     :date
#  data_base_ferias  :date
#  data_demissao     :date
#  data_fim_contrato :date
#  data_nascimento   :date
#  dtfim_ferias      :date
#  dtini_ferias      :date
#  email             :string
#  endereco          :string
#  escolaridade      :string
#  funcao            :string
#  gratificacao      :decimal(, )
#  matricula         :string
#  nome              :string
#  numfunc           :integer
#  numrg             :string
#  numvinc           :integer
#  orgao             :string
#  orgaorg           :string
#  pis               :string
#  poder             :string
#  referencia_cargo  :string
#  referencia_funcao :string
#  salario           :decimal(, )
#  sexo              :string
#  sigla_orgao       :string
#  situacao          :string
#  situacao_ferias   :string
#  telefone          :string
#  tipo_vinculo      :string
#  uf                :string
#  ufrg              :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryBot.define do
  factory :employee do
    matricula { "MyString" }
    numfunc { 1 }
    numvinc { 1 }
    nome { "MyString" }
    sexo { "MyString" }
    numrg { "MyString" }
    orgaorg { "MyString" }
    ufrg { "MyString" }
    cpf { "MyString" }
    data_admissao { "2018-10-26" }
    telefone { "MyString" }
    endereco { "MyString" }
    bairro { "MyString" }
    cidade { "MyString" }
    cep { "MyString" }
    uf { "MyString" }
    data_nascimento { "2018-10-26" }
    data_base_ferias { "2018-10-26" }
    email { "MyString" }
    cargo { "MyString" }
    funcao { "MyString" }
    orgao { "MyString" }
    cod_orgao { 1 }
    salario { "9.99" }
    gratificacao { "9.99" }
    tipo_vinculo { "MyString" }
    data_demissao { "2018-10-26" }
    data_fim_contrato { "2018-10-26" }
    escolaridade { "MyString" }
    situacao { "MyString" }
    referencia_cargo { "MyString" }
    pis { "MyString" }
    referencia_funcao { "MyString" }
    dtini_ferias { "2018-10-26" }
    dtfim_ferias { "2018-10-26" }
    sigla_orgao { "MyString" }
    agencia { "MyString" }
    conta { "MyString" }
    banco { "MyString" }
    situacao_ferias { "MyString" }
    cnpj_orgao { "MyString" }
    poder { "MyString" }
  end
end
