json.array!(@users) do |user|
  json.extract! user, :id, :name, :cpf, :matricula
  json.url user_url(user, format: :json)
end
