json.array!(@fake_secads) do |fake_secad|
  json.extract! fake_secad, :id, :user_id, :entity_id, :matricula, :numfunc, :numvinc, :sexo, :numrg, :orgaorg, :ufrg, :data_admissao, :telefone, :endereco, :bairro, :cidade, :cep, :uf, :data_nascimento, :data_base_ferias, :cargo, :funcao, :salario, :gratificacao, :tipo_vinculo, :data_demissao, :data_fim_contrato, :escolaridade, :situacao, :referencia_cargo_, :pis, :referencia_funcao_, :dtini_ferias, :dtfim_ferias, :agencia, :conta, :banco, :situacao_ferias
  json.url fake_secad_url(fake_secad, format: :json)
end
