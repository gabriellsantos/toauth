# == Schema Information
#
# Table name: adm_unid_gestora
#
#  id_unid_gestora   :integer          not null, primary key
#  desc_unid_gestora :string(255)      not null
#  sigla             :string(15)       not null
#  codigo            :integer          not null
#  gestor            :integer
#  nome_antigo       :string
#

class Entity < ActiveRecord::Base
	self.table_name = 'adm_unid_gestora'
	establish_connection "siato_public".to_sym

	def to_s
		nome
	end

	def nome
		if self.desc_unid_gestora == "-"
			return "#{self.nome_antigo} - INATIVO"
		end
		self.desc_unid_gestora
	end
end
