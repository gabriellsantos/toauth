# == Schema Information
#
# Table name: vw_servidor_ativo
#
#  id_servidor             :integer
#  matricula               :string(20)
#  numfunc                 :integer
#  numvinc                 :integer
#  nome                    :string(300)
#  desc_cidade             :string(50)
#  uf                      :string(2)
#  desc_situacao           :string(20)
#  desc_sexo               :string(1)
#  desc_cargo              :string(60)
#  desc_funcao             :string(60)
#  id_instituicao          :integer
#  id_nivel_funcional      :integer
#  rg                      :string(24)
#  rg_uf                   :string(2)
#  rg_orgao                :string(20)
#  cpf                     :string(11)       primary key
#  id_banco                :integer
#  agencia                 :string(8)
#  conta_corrente          :string(18)
#  data_admissao           :date
#  data_demissao           :date
#  fone                    :string(20)
#  fone2                   :string(14)
#  endereco                :string(60)
#  data_nascimento         :date
#  idade                   :text
#  email                   :string(100)
#  email_corporativo       :string(50)
#  data_base_ferias        :date
#  id_situacao_funcional   :integer
#  id_unid_reg             :integer
#  id_departamento         :integer
#  cd_banco                :string(5)
#  desc_banco              :text
#  desc_departamento       :text
#  sigla_dep               :string(25)
#  desc_instituicao        :text
#  sigla_instituicao       :string(15)
#  codigo                  :integer
#  desc_nivel_funcional    :string(100)
#  valor_nivel             :decimal(9, 2)
#  desc_situacao_funcional :string(50)
#  desc_unid_reg           :string(150)
#  desc_grau_escolaridade  :string(20)
#  motorista               :string(1)
#  cod_dep                 :string(15)
#  telefone                :string(25)
#  salario                 :decimal(10, 2)
#  gratificacao            :decimal(10, 2)
#  tipo_vinculo            :string(20)
#  ramal                   :string(15)
#  mes_aniversario         :float
#  referencia              :string(255)
#  referencia_funcao       :string(255)
#  pis                     :string(20)
#  id_banco_diaria         :integer
#  agencia_diaria          :string(18)
#  conta_corrente_diaria   :string(18)
#  dtini_ferias            :date
#  dtfim_ferias            :date
#  estagiario_colaborador  :integer
#  habilitacao             :string(15)
#  cod_motorista_garagem   :string(15)
#  cep                     :string(8)
#  cargo                   :string(60)
#  funcao                  :string(60)
#

class Siato< ActiveRecord::Base
	self.primary_key = "cpf"
	self.table_name = "vw_servidor_ativo"
	establish_connection "siato".to_sym


	def to_s
		self.nome
	end

end

