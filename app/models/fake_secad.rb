# == Schema Information
#
# Table name: fake_secads
#
#  id                 :integer          not null, primary key
#  agencia            :string
#  bairro             :string
#  banco              :string
#  cargo              :string
#  cep                :string
#  cidade             :string
#  conta              :string
#  data_admissao      :date
#  data_base_ferias   :date
#  data_demissao      :date
#  data_fim_contrato  :date
#  data_nascimento    :date
#  dtfim_ferias       :date
#  dtini_ferias       :date
#  endereco           :string
#  escolaridade       :string
#  funcao             :string
#  gratificacao       :string
#  matricula          :string
#  numfunc            :string
#  numrg              :string
#  numvinc            :string
#  orgao              :string(255)
#  orgaorg            :string
#  pis                :string
#  referencia_cargo_  :string
#  referencia_funcao_ :string
#  salario            :string
#  sexo               :string
#  situacao           :string
#  situacao_ferias    :string
#  telefone           :string
#  tipo_vinculo       :string
#  uf                 :string
#  ufrg               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  entity_id          :integer
#  user_id            :integer
#
# Indexes
#
#  index_fake_secads_on_entity_id  (entity_id)
#  index_fake_secads_on_user_id    (user_id)
#

class FakeSecad < ActiveRecord::Base
  belongs_to :user
  belongs_to :entity, primary_key: 'id_unid_gestora'

  def json_w_orgao
  	JSON.parse(self.to_json).merge(
  		{
  			cod_orgao: entity.id_unid_gestora, 
  			orgao: entity.to_s, 
  			sigla_orgao: entity.sigla,
  			cnpj_orgao: nil
  		}
  	)
  end

  def nome
    user.name
  end
end
