class Ability
	include CanCan::Ability

	def initialize(user)
        user ||= User.new # guest user (not logged in)
    	if user.admin?
            can :manage, :all
        elsif user.suporte?
            can :manage, User
    	else
    		can :minha_pagina, User
            can :password, User
            can :password_change, User
            can :change_avatar, User do |user1|
                user1 == user
            end
        end
    	can :api_dados, User
        can :senha_sucesso, User
        can :clear_coockie, User
    end
end
