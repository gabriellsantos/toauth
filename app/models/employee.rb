# == Schema Information
#
# Table name: employees
#
#  id                :integer          not null, primary key
#  matricula         :string
#  numfunc           :integer
#  numvinc           :integer
#  nome              :string
#  sexo              :string
#  numrg             :string
#  orgaorg           :string
#  ufrg              :string
#  cpf               :string
#  data_admissao     :date
#  telefone          :string
#  endereco          :string
#  bairro            :string
#  cidade            :string
#  cep               :string
#  uf                :string
#  data_nascimento   :date
#  data_base_ferias  :date
#  email             :string
#  cargo             :string
#  funcao            :string
#  orgao             :string
#  cod_orgao         :integer
#  salario           :decimal(, )
#  gratificacao      :decimal(, )
#  tipo_vinculo      :string
#  data_demissao     :date
#  data_fim_contrato :date
#  escolaridade      :string
#  situacao          :string
#  referencia_cargo  :string
#  pis               :string
#  referencia_funcao :string
#  dtini_ferias      :date
#  dtfim_ferias      :date
#  sigla_orgao       :string
#  agencia           :string
#  conta             :string
#  banco             :string
#  situacao_ferias   :string
#  cnpj_orgao        :string
#  poder             :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Employee < ApplicationRecord

  default_scope -> {order(nome: :asc)}

  def self.importar
    servidores = Secad.where(cod_orgao: 13)

    servidores.each do |s|
      servidor = Employee.find_or_create_by(cpf: s.cpf)
      servidor.matricula = s.matricula
      servidor.numfunc = s.numfunc
      servidor.numvinc = s.numvinc
      servidor.nome = s.nome
      servidor.sexo = s.sexo
      servidor.numrg = s.numrg
      servidor.orgaorg = s.orgaorg
      servidor.ufrg = s.ufrg
      servidor.cpf = s.cpf
      servidor.data_admissao = s.data_admissao
      servidor.telefone = s.telefone
      servidor.endereco = s.endereco
      servidor.bairro = s.bairro
      servidor.cidade = s.cidade
      servidor.cep = s.cep
      servidor.uf = s.uf
      servidor.data_nascimento = s.data_nascimento
      servidor.data_base_ferias = s.data_base_ferias
      servidor.email = s.email
      servidor.cargo = s.cargo
      servidor.funcao = s.funcao
      servidor.orgao = s.orgao
      servidor.cod_orgao = s.cod_orgao
      servidor.salario = s.salario
      servidor.gratificacao = s.gratificacao
      servidor.tipo_vinculo = s.tipo_vinculo
      servidor.data_demissao = s.data_demissao
      servidor.data_fim_contrato = s.data_fim_contrato
      servidor.escolaridade = s.escolaridade
      servidor.situacao = s.situacao
      servidor.referencia_cargo = s.referencia_cargo_
      servidor.pis = s.pis
      servidor.referencia_funcao = s.referencia_funcao_
      servidor.dtini_ferias = s.dtini_ferias
      servidor.dtfim_ferias = s.dtfim_ferias
      servidor.sigla_orgao = s.sigla_orgao
      servidor.agencia = s.agencia
      servidor.conta = s.conta
      servidor.banco = s.banco
      servidor.situacao_ferias = s.situacao_ferias
      servidor.cnpj_orgao = s.cnpj_orgao

      servidor.save

    end
  end
end
