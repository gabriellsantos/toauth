# == Schema Information
#
# Table name: serv_servidor
#
#  id                                 :integer          not null, primary key
#  agencia                            :string(20)
#  bairro                             :string(72)
#  banco                              :string(20)
#  cargo                              :string(60)
#  cep                                :string(8)
#  cidade                             :string(50)
#  cnpj_orgao                         :string(255)
#  cod_orgao                          :integer
#  conta                              :string(20)
#  cpf                                :string(11)
#  data_admissao                      :date
#  data_base_ferias                   :date
#  data_demissao                      :date
#  data_fim_contrato                  :date
#  data_nascimento                    :date
#  dtfim_ferias(Data fim férias)      :date
#  dtini_ferias(Data início férias)   :date
#  email                              :string(100)
#  endereco                           :string(60)
#  escolaridade                       :string(20)
#  funcao                             :string(60)
#  gratificacao                       :decimal(10, 2)
#  last_update                        :date
#  matricula                          :string(20)
#  nome                               :string(300)
#  numfunc                            :integer
#  numrg                              :string(24)
#  numvinc                            :integer
#  orgao                              :string(50)
#  orgaorg                            :string(20)
#  pis                                :string(20)
#  referencia_cargo_(Nível Funcional) :string(255)
#  referencia_funcao_                 :string(255)
#  salario                            :decimal(10, 2)
#  sexo                               :string(1)
#  sigla_orgao(Sigla do Órgão)        :string(15)
#  situacao                           :string(20)
#  situacao_ferias                    :string(255)
#  telefone                           :string(20)
#  tipo_vinculo                       :string(20)
#  uf                                 :string(2)
#  ufrg                               :string(2)
#

class Secad < ActiveRecord::Base
	self.table_name = 'serv_servidor'
	establish_connection "secad".to_sym

	default_scope -> {where(situacao: "ATIVO")}

	def to_s
		self.nome
	end
end
