# == Schema Information
#
# Table name: serv_departamento
#
#  cod_dep                                                                                      :string(15)       default("0")
#  ddd                                                                                          :string(2)
#  desc_departamento                                                                            :string(200)
#  email                                                                                        :string(100)
#  id_departamento                                                                              :integer          not null, primary key
#  id_departamento_superior(Departamento superior)                                              :integer
#  id_servidor                                                                                  :integer
#  id_servidor_suplente(Responsável suplente)                                                   :integer
#  id_situacao                                                                                  :integer
#  id_unid_gestora                                                                              :integer
#  id_unid_reg                                                                                  :integer
#  is_isento(Campo que diz se o chefe deste departamento é isento de autorização de sua viagem) :integer
#  nivel                                                                                        :integer
#  sigla_dep                                                                                    :string(25)
#  skype_dep                                                                                    :string(40)
#  telefone                                                                                     :string(25)
#
# Foreign Keys
#
#  serv_departamento_unid_gestora_fkey  (id_unid_gestora => public.adm_unid_gestora.id_unid_gestora)
#  serv_departamento_unid_reg_fkey      (id_unid_reg => public.adm_unid_reg.id_unid_reg)
#

class Department < ActiveRecord::Base
	self.primary_key = "id_departamento"
	self.table_name = "serv_departamento"
	establish_connection "siato".to_sym

	def to_s
		self.desc_departamento
	end
end

