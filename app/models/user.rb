# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  admin                  :boolean
#  bypass_validation      :boolean
#  cloudinary_code        :string
#  cpf                    :string
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :string
#  matricula              :string
#  name                   :string
#  photo                  :text
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  sign_in_count          :integer          default(0), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
	require 'openssl'
	require 'base64'
	require 'rest-client'
	extend Enumerize

	has_one :fake_secad

	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
	:recoverable, :rememberable, :trackable, :validatable

	validate :validate_secad
	validates :cpf, :matricula, presence: true
	validates :cpf, uniqueness: true
	before_validation :set_name
	before_validation :check_bypass, on: :create

	accepts_nested_attributes_for :fake_secad

	enumerize :perfil, in: [:admin, :suporte, :servidor], predicates: true, default: :servidor

	attr_accessor :login

	default_scope -> {where(bypass_validation: [nil, true])}

	def self.base64_to_hex(base64_string)
		base64_string.scan(/.{4}/).map do |b|
			b.unpack('m0').first.unpack('H*')
		end.join
	end

	def self.teste(msg)

	end

	def self.decrypt(msg)
		#begin
			cipher = OpenSSL::Cipher.new('AES-128-ECB')
			cipher.decrypt()

			cipher.key = "At1_SgF@gds2020#" #"1234567890098765"
			#converte o hexadecimal em base64
			hex_2_64 = [[msg].pack("H*")].pack("m0")
			tempkey = Base64.decode64(hex_2_64)
			crypt = cipher.update(tempkey) + cipher.final
			#crypt << cipher.final()
			return crypt
			#rescue Exception => exc
			#puts ("Message for the decryption log file for message #{msg} = #{exc.message}")
			#end
	end

	def profile
		secad =  retornaDadosServidor #Secad.where(cpf: self.cpf).last #Employee.find_by_cpf(self.cpf)
		if secad.blank?
			return fake_secad
		end
		secad
	end

	#Busca os dados do servidor na EstruturaApi
	def retornaDadosServidor
		
		rest = RestClient.get 'http://estrutura.ati.to.gov.br/departments/retornaDadosServidor', {params: {cpf: self.cpf, token: "314abca0b4d1b6c5cb336fa1bbc09c8e6a28a0733fcd068b5281f4819488bb7d"}}
		begin
			if rest.present?
				if JSON.parse(rest).size == 2
					JSON.parse(rest)[0].with_indifferent_access
				else
					return JSON.parse(rest).with_indifferent_access
				end
			else
				{}
			end
		rescue StandardError => e
			puts e.message  
  		puts e.backtrace.inspect  
		end
	end

	def can_fake_secad
		Secad.find_by_cpf(self.cpf).blank?
	end

	def siato
		Siato.find_by_cpf(self.cpf)
	end

	def departamento
		#if siato.present? and siato.id_departamento.present?
			#return Department.find(siato.id_departamento)
		#end
		retorno = {}

		begin
			rest = RestClient.get 'http://estrutura.ati.to.gov.br/departments/departamentoServidor', {params: {cpf: self.cpf, token: "314abca0b4d1b6c5cb336fa1bbc09c8e6a28a0733fcd068b5281f4819488bb7d"}}
			if rest.present?
				retorno = JSON.parse(rest)
			end
		rescue
			retorno = {}
		end
		retorno
	end

	def boss?
		if departamento.present?			
			return departamento.id_servidor == siato.id_servidor
		end
		false
	end

	def responsaveis_departamento
		dep = departamento
		if dep.present?
			begin
				responsaveis = RestClient.get 'http://estrutura.ati.to.gov.br/departments/responsaveisAtivos', {params: {codigo: dep[0]["department_id"], token: "314abca0b4d1b6c5cb336fa1bbc09c8e6a28a0733fcd068b5281f4819488bb7d"}}
				JSON.parse(responsaveis)
			rescue
				{}
			end
		else
			{}
		end
	end

	def chefe_imediato
		dep = departamento
		if dep.present?
			begin
				chefe1 = Siato.find_by_id_servidor(departamento.id_servidor)
				if chefe1.cpf == self.cpf
					next_department = Department.find(departamento.id_departamento_superior)
					return Siato.find_by_id_servidor(next_department.id_servidor)
				end
				return chefe1
			rescue
				{}
			end
		end
		{}
	end

	def chefe_suplente
		begin
			if departamento.present?
				chefe_imediato = Siato.find_by_id_servidor(departamento.id_servidor)
				chefe1 = Siato.find_by_id_servidor(departamento.id_servidor_suplente)
				if chefe_imediato.cpf == self.cpf
					next_department = Department.find(departamento.id_departamento_superior)
					return Siato.find_by_id_servidor(next_department.id_servidor_suplente)
				end
				return chefe1
			end
		rescue
		end
		{}
	end

	def set_name
		puts "cpf - #{self.cpf}"
        if !can_fake_secad
            begin
            self.name = self.profile["nome"]
            rescue
            end
		end
	end

	def name_top
		if self.name.present?
			self.name.split(" ")[0]
		else
			"Usuário"
		end		
	end

	def check_bypass
		if bypass_mail?
			self.bypass_validation = false
			self.name = 'Usuário Unitins'
			self.fake_secad = FakeSecad.new(
				 entity_id: 1001
			)
		end
	end

	def bypass_config
		[
				{mail: '@unitins.br', entity: '1001', cargo: 'unitins' }
		]
	end

	def bypass_mail?
		bypass_config.any? { |word| self.email.include?(word[:mail]) }
	end

	def validate_secad
		if !bypass_mail?
			if profile.blank?
					errors.add(:profile, "Você não possui cadastro ativo na SECAD")
			else
					if fake_secad.blank? and profile["numfunca"] != self.matricula.to_i
						errors.add(:profile, "Os dados cadastrados não estão de acordo com os dados da SECAD.")
					end
			end
		end
	end

	def active?
		profile.present?
	end


	def photo_url
		if self.photo.present?
			self.photo.sub! 'http://', 'https://'
		else
			'http://toauth.ati.to.gov.br/mosaicpro/images/default-avatar.png'
		end
	end

	def self.find_for_database_authentication(warden_conditions)
		conditions = warden_conditions.dup
		if login = conditions.delete(:login)
			where(conditions.to_h).where(["lower(cpf) = :value OR lower(email) = :value", { :value => login.downcase }]).first
		else
			where(conditions.to_h).first
		end
	end

	def cordinates
		Geocoder.coordinates("#{profile["endereco"]}, #{profile["cidade"]}, Tocantins, Brasil")
	end
end