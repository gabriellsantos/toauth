class FakeSecadsController < ApplicationController
  before_action :set_fake_secad, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @fake_secads = FakeSecad.all
    respond_with(@fake_secads)
  end

  def show
    respond_with(@fake_secad)
  end

  def new
    @fake_secad = FakeSecad.new
    respond_with(@fake_secad)
  end

  def edit
  end

  def create
    @fake_secad = FakeSecad.new(fake_secad_params)
    @fake_secad.save
    respond_with(@fake_secad)
  end

  def update
    @fake_secad.update(fake_secad_params)
    respond_with(@fake_secad)
  end

  def destroy
    @fake_secad.destroy
    respond_with(@fake_secad)
  end

  private
    def set_fake_secad
      @fake_secad = FakeSecad.find(params[:id])
    end

    def fake_secad_params
      params.require(:fake_secad).permit(:user_id, :entity_id, :matricula, :numfunc, :numvinc, :sexo, :numrg, :orgaorg, :ufrg, :data_admissao, :telefone, :endereco, :bairro, :cidade, :cep, :uf, :data_nascimento, :data_base_ferias, :cargo, :funcao, :salario, :gratificacao, :tipo_vinculo, :data_demissao, :data_fim_contrato, :escolaridade, :situacao, :referencia_cargo_, :pis, :referencia_funcao_, :dtini_ferias, :dtfim_ferias, :agencia, :conta, :banco, :situacao_ferias)
    end
end
