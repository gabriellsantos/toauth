class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  # GET /employees
  # GET /employees.json
  def index
    if current_user.admin
      @q = Employee.ransack(params[:q])
    else
      @q = Employee.where(cod_orgao: current_user.profile.cod_orgao).ransack(params[:q])
    end

    @employees = @q.result.page(params[:page]).per(50)
    render layout: "material"
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    render layout: "material"
  end

  # GET /employees/new
  def new
    @employee = Employee.new
    render layout: "material"
  end

  # GET /employees/1/edit
  def edit
    render layout: "material"
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:matricula, :numfunc, :numvinc, :nome, :sexo, :numrg, :orgaorg, :ufrg, :cpf, :data_admissao, :telefone, :endereco, :bairro, :cidade, :cep, :uf, :data_nascimento, :data_base_ferias, :email, :cargo, :funcao, :orgao, :cod_orgao, :salario, :gratificacao, :tipo_vinculo, :data_demissao, :data_fim_contrato, :escolaridade, :situacao, :referencia_cargo, :pis, :referencia_funcao, :dtini_ferias, :dtfim_ferias, :sigla_orgao, :agencia, :conta, :banco, :situacao_ferias, :cnpj_orgao, :poder)
    end
end
