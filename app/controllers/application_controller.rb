class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exceptbefore_action :configure_permitted_parameters, if: :devise_controller?ion.
  # For APIs, you may want to use :null_session instead:insufficient_storage.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    cookies.signed[:user_id] = { value: current_user.id, expires: 2.day.from_now }
  	(session[:"user.return_to"].nil?) ? "/" : session[:"user.return_to"].to_s
  end

  rescue_from CanCan::AccessDenied do |exception|
  	redirect_to root_url, :alert => "Você não está autorizado para acessar esta página"
  end

  protected

	 def configure_permitted_parameters
	    devise_parameter_sanitizer.permit(:sign_up, keys: [:cpf, :matricula, :email, :login])
	 end
end
