class UsersController < ApplicationController

  skip_before_action :verify_authenticity_token, only:[:autenticar_service, :login_secad]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :change_avatar]
  before_action :authenticate_user!, except: [:senha_sucesso , :clear_coockie,:autenticar_service, :login_secad]
  #before_action :allow_iframe, :set_headers
  load_and_authorize_resource except:[:autenticar_service, :login_secad]


  respond_to :html, :json

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET, POST'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = '*'
  end

  #Método que realiza o login do usuário passado pelo portal do servidor
  def login_secad
    #https://www.devglan.com/online-tools/aes-encryption-decryption
    cpf = User.decrypt(params[:data])
    user = User.find_by_cpf(cpf)
    if user.present?
      sign_in(user)
      redirect_to "http://sgf-hom.ati.to.gov.br"
      return
    else
      user = User.new(cpf: cpf)
      dados = user.retornaDadosServidor
      if dados.present?
        user.email = dados[:email]
        user.matricula = "#{dados[:numfunca]}-#{dados[:numvinc]}"
        user.name = dados[:nome]
        password = rand(99999999)
        user.password = password
        user.password_confirmation = password
        if user.save
          sign_in(user)
          redirect_to "http://sgf-hom.ati.to.gov.br"
          return
        else
          redirect_to root_path, notice: "Erro ao criar usuário!"
          return
        end
      else
        redirect_to root_path, notice: "Servidor não encontrado!"
        return
      end
    end

  end

  #Método para autenticar via serviço
  def autenticar_service

    user = User.find_by(cpf: params[:cpf])
    #binding.pry
    if user.present?
      if user.valid_password? params[:password]
        render json: {status: "OK", user: user.profile.reject{ |p| p["biometria"]}, data_hora: DateTime.now}
      else
        render json: {status: "ERROR", mensagem: "Dados de acesso inválidos."}
      end
    else
      render json: {status: "ERROR", mensagem: "Servidor não encontrado."}
    end

  end

  def index
    @q = User.unscoped.ransack(params[:q])
    @users = @q.result(distinct: true).page(params[:page]).per(10)
    respond_with(@users)
  end

  def minha_pagina
    @user = current_user
    render :show, layout: "material"
  end

  def show
    render layout: "material"
  end

  def new
    @user = User.new
    @user.fake_secad = FakeSecad.new
    respond_with(@user)
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    @user.save(validates: false)
    respond_with(@user)
  end

  def update
    @user.update(user_params)
    respond_with(@user)
  end

  def destroy
    @user.destroy
    respond_with(@user)
  end

  def password
  end

  def password_change
    user = current_user
    user.password = params[:password]
    user.password_confirmation = params[:password_confirmation]
    if user.save
      redirect_to senha_sucesso_path
    else
      redirect_to minha_senha_path, :flash => { :alert => "Não foi possível salvar sua senha, tente novamente." }
    end
  end

  def senha_sucesso
  end

  def change_avatar
    if params[:user][:photo].present?
      if @user.photo.present?
        code_antigo = @user.cloudinary_code || "toauth_avatar_#{@user.cpf}"
        Cloudinary::Uploader.destroy(code_antigo)
      end
      @user.cloudinary_code = "toauth_avatar_#{@user.cpf}_#{DateTime.now.to_i}"
      Cloudinary::Uploader.upload(params[:user][:photo], public_id: "#{@user.cloudinary_code}", invalidate: true)
      @user.photo = Cloudinary::Utils.cloudinary_url("#{@user.cloudinary_code}", width: 100, height: 100, crop: :fill, gravity: :face)
      @user.save
    end 
    redirect_to root_path
  end

  def clear_coockie
    cookies.delete :user_id
    redirect_to "/"
  end

  private
    def set_user
      @user = User.unscoped.find(params[:id])
    end

    def user_params
      user_params = params.require(:user).permit(
        :name, :cpf, :matricula,:password, :photo, :perfil, :bypass_validation,
        fake_secad_attributes:[
         :id, :user_id, :entity_id, :matricula, :numfunc, :numvinc, :sexo, 
         :numrg, :orgaorg, :ufrg, :data_admissao, :telefone, :endereco, :bairro,
         :cidade, :cep, :uf, :data_nascimento, :data_base_ferias, :cargo, 
         :funcao, :salario, :gratificacao, :tipo_vinculo, :data_demissao, 
         :data_fim_contrato, :escolaridade, :situacao, :referencia_cargo_, 
         :pis, :referencia_funcao_, :dtini_ferias, :dtfim_ferias, :agencia, 
         :conta, :banco, :situacao_ferias
        ]
      )

      user_params.delete(:password) unless user_params[:password].present?
      user_params.delete(:password_confirmation) unless user_params[:password_confirmation].present?

      user_params
    end

end
