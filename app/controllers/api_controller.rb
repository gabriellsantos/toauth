class ApiController < ApplicationController
	before_action :doorkeeper_authorize!, only: [:dados_api]
	skip_before_action :verify_authenticity_token

	def me
		@user = User.find(doorkeeper_token.resource_owner_id)
		render 'users/show' #show.json
	end

	def get_token
		#binding.pry
		user = OAuth2::Client.new(params[:client_id], params[:client_secret], site:  "http://localhost:3000")
		if user.blank?
			return {error: true}
		end
		token = user.auth_code.get_token(params[:code], redirect_uri: params[:redirect_uri])
		render json: {access_token: token.token}
	end



	def profile
		@user = User.find(doorkeeper_token.resource_owner_id)
		render 'users/show_complete'
	end

	def departments_by_ug
		@departments = Department.where(id_unid_gestora: params[:ug], id_situacao: 1)
		render json: @departments
	end

	def servers_by_ug
		@servers = Siato.where(id_instituicao: params[:ug])
		render json: @servers
	end

	def sign_by_password
		@user = User.find_by_cpf(params[:cpf])
		if @user.present? and @user.valid_password?(params[:password])
			@token = Doorkeeper::AccessToken.create(
				resource_owner_id: @user.id
			)
			response = RestClient.post(
				"http://assine.seplan.to.gov.br/signatures",
				{
					access_token: @token.token,
					signature: params[:signature]
				}
			)
			render json: {signature: JSON.parse(response), status: true}
		else
			render json: {status: false}
		end
	end
end
